## 0.2.2 (2023-07-02)
### fix
- [[`7cac9ae`](https://gitlab.com/katalytic/katalytic-maths/commit/7cac9ae0ac2f780c1d1682d2b9a4d245bf2a1a43)] TypeError in the is_bbox() function
- [[`848919a`](https://gitlab.com/katalytic/katalytic-maths/commit/848919ac7e63f34bc9defa9e41a17c72e734dbe7)] TypeError in the is_bbox() function
- [[`856758d`](https://gitlab.com/katalytic/katalytic-maths/commit/856758dde07b8e06fac9cfbc74cfa3663d56adf8)] calling pop_max() with 'key' instead of 'condition'
- [[`30b0965`](https://gitlab.com/katalytic/katalytic-maths/commit/30b096524f33ba7249170fd67b5ef75a68448d98)] improve the is_bbox() helper functions and add docstrings to every function in the module


## 0.2.1 (2023-07-02)
### fix
- [[`7cac9ae`](https://gitlab.com/katalytic/katalytic-maths/commit/7cac9ae0ac2f780c1d1682d2b9a4d245bf2a1a43)] TypeError in the is_bbox() function
- [[`848919a`](https://gitlab.com/katalytic/katalytic-maths/commit/848919ac7e63f34bc9defa9e41a17c72e734dbe7)] TypeError in the is_bbox() function
- [[`856758d`](https://gitlab.com/katalytic/katalytic-maths/commit/856758dde07b8e06fac9cfbc74cfa3663d56adf8)] calling pop_max() with 'key' instead of 'condition'
- [[`30b0965`](https://gitlab.com/katalytic/katalytic-maths/commit/30b096524f33ba7249170fd67b5ef75a68448d98)] improve the is_bbox() helper functions and add docstrings to every function in the module


## 0.2.0 (2023-05-06)
### feat
- [[`526f61a`](https://gitlab.com/katalytic/katalytic-maths/commit/526f61afcabab4f37c530cbf4e18ae5ebe01af9d)] add functions for bboxes


## 0.1.0 (2023-05-02)
### feat
- [[`a5a76fc`](https://gitlab.com/katalytic/katalytic-maths/commit/a5a76fcf6a11ace8f1c8a42cb59e281638dc2a78)] add L1, L2, min_max, and clip
### fix
- [[`da40700`](https://gitlab.com/katalytic/katalytic-maths/commit/da40700a0443156aabd69d78945d805a17210bac)] **min_max:** 'NoneType' object is not callable


## 0.1.3 (2023-04-14)
### Fix
* Release ([`b05f06a`](https://github.com/katalytic/katalytic-images/commit/b05f06a0562caaaecb3ae78dd6167f6efd7bfdd9))


## 0.1.2 (2023-04-14)
### Fix
* Release ([`d8dcc26`](https://github.com/katalytic/katalytic-images/commit/d8dcc26a40c78db399546ed6b03d759de46c5368))
* Release ([`ca4c3a4`](https://github.com/katalytic/katalytic-images/commit/ca4c3a46dc9c845be9829176346a1a5e14c7eb09))
* Prep for travis ([`4533802`](https://github.com/katalytic/katalytic-images/commit/45338021cb605202ec63645780951545a28408e9))
* Prep for travis ([`a7ab07a`](https://github.com/katalytic/katalytic-images/commit/a7ab07abb5e2bbf5001f85039820ced1cbeec541))


